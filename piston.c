/*
 * piston power off daemon for Raspberry Pi
 * 
 * Copyright 2014 Vik. vik.public _O_ gmail.com 
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <syslog.h>

#if !defined MAX_PATH
#define MAX_PATH	256
#endif

typedef unsigned char	bool;
typedef unsigned int	gpio_number;

#define false	((bool)0)
#define true	((bool)~0)

static gpio_number	input_pin = 4;	/* pin number 7 on the connector */

static char path_buffer[ MAX_PATH ] = { 0 };
static char exported = false;

/*===========================================================
 *
 */
static bool create_file_name( gpio_number pin, char const* operation )
{
    return 0 <= snprintf( 
	path_buffer, sizeof(path_buffer)/sizeof(path_buffer[0]), 
	"/sys/class/gpio/gpio%d/%s", pin, operation );
}


/*===========================================================
 *
 */
static bool get_pin_value( gpio_number pin, bool *value )
{
    bool res = false;
    FILE* fd = 0;

    if ( !create_file_name( pin, "value" ))
    {
	syslog( LOG_ERR, "Can\'t open create value file name" );
    }
    else if ( !( fd = fopen( path_buffer, "r" )))
    {
	syslog( LOG_ERR, "Can\'t open value file" );
    }
    else
    {
	char input[16] = { 0 };
	if ( 0 >= fread( input, 1, sizeof(input)/sizeof(input[0]), fd ))
	{
	    syslog( LOG_ERR, "Can\'t read pin value");
	}
	else
	{
	    input[ sizeof(input)/sizeof(input[0]) - 1 ] = 0;
	    *value = ( 0 != atoi( input ));
	    res = true;
	}
	fclose(fd);
    }
    return res;
}

/*===========================================================
 *
 */
static bool set_pin_direction( gpio_number pin, bool is_direction_in )
{
    bool res = false;
    FILE* fd = 0;

    if ( !create_file_name( pin, "direction" ))
    {
	syslog( LOG_ERR, "Can\'t open create direction file name" );
    }
    else if ( !( fd = fopen( path_buffer, "w" )))
    {
	syslog( LOG_ERR, "Can\'t open direction file" );
    }
    else
    {
	if ( 0 >= fprintf(fd, "%s", ( is_direction_in ? "in" : "out" )))
	{
	    syslog( LOG_ERR, "Can\'t set pin direction" );
	}
	else
	{
	    res = true;
	}
	fclose(fd);
    }
    return res;
}

/*===========================================================
 *
 */
static bool export_pin( gpio_number pin )
{
    bool res = false;
    FILE* fd = fopen( "/sys/class/gpio/export", "w");
    if ( !fd )
    {
	syslog( LOG_ERR, "Can\'t open export file" );
    }
    else
    {
	if ( 0 >= fprintf( fd, "%d", pin )) 
	{
	    syslog( LOG_ERR, "Can\'t export pin" );
	}
	else
	{
	    res = true;
	}
	fclose(fd);
    }
    return res;
}

/*===========================================================
 *
 */
static bool unexport_pin( gpio_number pin )
{
    bool res = false;
    FILE* fd = fopen( "/sys/class/gpio/unexport", "w" );
    if ( !fd )
    {
	syslog( LOG_ERR, "Can\'t open unexport file" );
    }
    else
    {
	if ( 0 >= fprintf( fd, "%d", pin )) 
	{
	    syslog( LOG_ERR, "Can\'t unexport pin" );
	}
	else
	{
	    res = true;
	}
	fclose( fd );
    }
    return res;
}


/*===========================================================
 *
 */
static void on_exit_signal( int signal )
{
    if ( exported )
    {
	unexport_pin( input_pin );
	exported = false;
    }
    closelog();
}

/*===========================================================
 *===========================================================
 */
int main( int argc, char** argv ) {
    pid_t pid, sid;

    pid = fork();
    if ( 0 > pid )
    {
	exit ( EXIT_FAILURE );
    }

    if ( 0 < pid )
    {
	exit ( EXIT_SUCCESS );
    }
    umask(0);

    /* open logs */

    openlog("poweroff", LOG_NOWAIT | LOG_PID, LOG_USER );
    syslog( LOG_INFO, "Started sucessfully" );

    sid = setsid();
    if ( 0 > sid )
    {
	syslog( LOG_ERR, "got wrong session id");
	exit (EXIT_FAILURE);
    }

    if ( 0 > ( chdir( "/" )))
    {
	syslog( LOG_ERR, "failed to change directory");
	exit ( EXIT_FAILURE );
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);


    int res = 0;
    exported = false;

    do
    {
	signal( SIGABRT, on_exit_signal );
	signal( SIGSEGV, on_exit_signal );
	signal( SIGABRT, on_exit_signal );
	signal( SIGFPE,  on_exit_signal );
	signal( SIGINT,  on_exit_signal );

	exported = export_pin( input_pin );
	if ( !exported ) {
	    syslog( LOG_ERR, "Can\'t export input pin [%d]", input_pin );
	    res = -1;
	    break;
	}

	sleep( 1 );

	if ( !set_pin_direction( input_pin, true ))
	{
	    res = -2;
	    break;
	}
	
	bool value = true;
	while ( exported && value && get_pin_value( input_pin, &value ))
	{
	    /*
	    printf( "input value is: %s\n", value ? "not pressedd" : "pressed" );
	    */
	    if ( !value )
	    {
		system( "sudo /sbin/halt" );
	    }
	    else
	    {
		sleep( 1 );
	    }
	}

    } while (0);

    if ( exported ) {
	unexport_pin( input_pin );
	exported = false;
    }
    closelog();

    exit ( EXIT_SUCCESS );
}