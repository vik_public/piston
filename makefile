all:	install

piston:
	cc -O4 -s piston.c -o piston


install: piston
	if [ -f piston ] ; then \
	    cp ./piston /usr/local/bin/piston ; \
	    cp ./init.d/piston /etc/init.d ; \
	    update-rc.d piston defaults ; \
	    update-rc.d piston enable; \
	    update-rc.d piston start; \
	fi ;
