# PiSton #

PisTon is a small project which targets simple problem to solve: How to shutdown Raspberry Pi, when there is neither screen no keyboard coonnected. 

The project consists on two parts:

* Simple hardware button, which can be connected to any of the GPIO pin.
* Simple daemon, which uses udev interface to get access to the used pin state. This daemon pools all the time the button state. Period is approximately 1 sec - it should not hurt the overall system performance. After button is pressed, the daemon calls "sudo /sbin/halt". So. This command should be allowed for daemon in the sudoers file.

### Current version ###

* First release. Stable, while working on my RPi during 6 months.
* 1.0
* This is initial release. The daemon source code, simple make file, SysV start script.

### How do I get set up? ###

* Installation:
    *  Download the last source code from repository.
    *  Call 'make piston' in the directory where a makefile placed to compile the binary.
    *  Call 'sudo make install' to install the piston binary to the /usr/local/bin and _sysv_ start script to the /etc/init.d  directory and register it in SysV start-up sequence.
    *  Using 'sudo visudo' add appropriate record to the sudoers file:
<owner of the /usr/local/bin/piston> ALL=(ALL) NOPASSWD: /sbin/halt

* there is no special configuration options available. The used pin number can be changed in source file: 
```
#!C++
static gpio_number input_pin = 4; /* pin number 7 on the connector */
```
This means, that pin 7 in the GPIO header is input pin 4 for BCM2835 processor. Check GPIO header pin meanings in the network.

### Dependencies ###
This project depends on practically nothing, while it's using standard udev service, which is available in pretty every linux kernel nowadays. I'm using Raspbian and that is why I did SysV init script to start the daemon. The one can create systemd service file, I will gracefully put it there.

### Contribution guidelines ###
While the project is not pretend to be really popular, I will probably just check every incoming change. At least onteh first time.This politics can be changed later.

### Who do I talk to? ###

* vik.public@gmail.com